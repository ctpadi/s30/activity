const express = require("express");
const mongoose = require("mongoose");
const app = express();
const port = 4000;
mongoose.connect("mongodb+srv://admin:admin123@course-booking.0inwk.mongodb.net/S30?retryWrites=true&w=majority", 
	{
		useNewUrlParser:true,
		useUnifiedTopology: true
});

let db = mongoose.connection;

db.on("error", console.error.bind(console, "Connection error"));

db.once("open", () => console.log("We're connected to the cloud database"));
const userSchema = new mongoose.Schema({
	username: String,
	password: String,
	status: {
		type: String,
		default: "pending"
	}

});


const User = mongoose.model("User", userSchema);
app.use(express.json());
app.use(express.urlencoded({extended:true}));
app.post("/signup", (req, res) => {
	User.findOne({name: req.body.username}, (err, result) => {
		if (result != null && result.username == req.body.username){
			return res.send("Username taken");
		} else {
			let newUser = new User({
				username: req.body.username,
				password: req.body.password
			});
			newUser.save((saveErr, savedTask) => {
				if(saveErr){
					return console.error(saveErr)
				} else {
					return res.status(201).send("New user registered")
				}

			})

		}

	})

})


app.listen(port, () => console.log(`Server running at port ${port}`));